package edu.manyaneelu.contactsapp.io;

import java.util.List;

import edu.manyaneelu.contactsapp.ContactInformation;

public interface IInputOutput
{
	void createConnection(String s) throws Exception;
	void createConnection(String url, String username, String password)throws Exception;
	void save(ContactInformation ref) throws Exception;
	List<String> printAll() throws Exception;
	void closeConnection() throws Exception;
	
}
