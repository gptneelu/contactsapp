/**
 * 
 */
package edu.manyaneelu.contactsapp.io.file;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import edu.manyaneelu.contactsapp.ContactInformation;
import edu.manyaneelu.contactsapp.io.IInputOutput;

/**
 * @author NEELU
 *
 */
public class Serial implements IInputOutput {
	ObjectOutputStream out;
	ObjectInputStream in;
	
	/**
	 * 
	 */
	public Serial() 
	{
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see edu.manyaneelu.contactsapp.io.IInputOutput#createConnection(java.lang.String)
	 */
	@Override
	public void createConnection(String s) throws Exception
	{
		out=new ObjectOutputStream(new FileOutputStream(s));	//true for append mode
		in=new ObjectInputStream(new FileInputStream(s));
		
	}

	/* (non-Javadoc)
	 * @see edu.manyaneelu.contactsapp.io.IInputOutput#save(edu.manyaneelu.contactsapp.ContactInformation)
	 */
	@Override
	public void save(ContactInformation ref) throws Exception
	{
		out.writeObject(ref);

	}

	/* (non-Javadoc)
	 * @see edu.manyaneelu.contactsapp.io.IInputOutput#printAll()
	 */
	@Override
	public List<String> printAll() throws Exception 
	{
		ContactInformation o;
		ArrayList<String> ar=new ArrayList<String>();
		try
		{
			while((o=(ContactInformation)in.readObject())!=null)
			{
				ar.add(o.getName()+" "+o.getPhoneNo()+" "+o.getEmail());
			}
		}
		catch(EOFException e)
		{
			
		}
		return ar;
	}

	/* (non-Javadoc)
	 * @see edu.manyaneelu.contactsapp.io.IInputOutput#closeConnection()
	 */
	@Override
	public void closeConnection() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void createConnection(String url, String username, String password) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
