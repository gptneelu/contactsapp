package edu.manyaneelu.contactsapp.io.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import edu.manyaneelu.contactsapp.ContactInformation;
import edu.manyaneelu.contactsapp.io.IInputOutput;

public class Database implements IInputOutput {

	Connection con;
	Statement stmt;
	
	public Database() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void createConnection(String s) throws Exception {
		// TODO Auto-generated method stub

		
	}

	@Override
	public void save(ContactInformation ref) throws Exception {
		// TODO Auto-generated method stub
		StringBuilder str = new StringBuilder("Insert Into Contact Values(");
		str.append("'"+ref.getName()+"',");
		str.append("'"+ref.getPhoneNo()+"',");
		str.append("'"+ref.getEmail()+"')");
		stmt.executeUpdate(str.toString());

	}

	@Override
	public List<String> printAll() throws Exception {
		// TODO Auto-generated method stub
		ContactInformation c;
		ArrayList<String> ar = new ArrayList<>();
		
		ResultSet set = stmt.executeQuery("Select * from Contact");
		
		while(set.next())
		{
			ar.add(set.getString(1)+" "+set.getBigDecimal(2).toString()+" "+set.getString(3));
		}
		
		return ar;
	}

	@Override
	public void closeConnection() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void createConnection(String url, String username, String password) throws Exception {
		
		Class.forName("com.mysql.jdbc.Driver");  
//		con=DriverManager.getConnection("jdbc:mysql://localhost:3306/test","root","root");
		con=DriverManager.getConnection(url,username,password);
		//here sonoo is database name, root is username and password  
		stmt=con.createStatement(); 
	}

}
